import React from "react";
import "./App.css";
import Header from "./components/header";
import Form from "./components/formCreate";
import Section from "./components/section";
import Add from "./components/formAdd";
import { useSelector } from "react-redux";

function App() {
  const [opened, data] = useSelector((state) => [state.opened, state.data]);

  return (
    <div className="container">
      <Header />
      <Form />
      <Section data={data} />
      <Add opened={opened} />
    </div>
  );
}

export default App;
