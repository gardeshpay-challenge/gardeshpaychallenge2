import React from "react";
import "./css/section.css";

export default function Section(props) {
  return (
    <section>
      <table>
        <thead>
          <tr>
            {tableHeader.map((th, index) => (
              <th>
                {th.header}
                <i className="fa fa-sort"></i>
              </th>
            ))}
          </tr>
        </thead>

        <tbody>
          <tr>
            {Object.keys(props.data).map((key, index) => (
              <td key={`field-${index}`}>{props.data[key]}</td>
            ))}
          </tr>
        </tbody>
      </table>
    </section>
  );
}

const tableHeader = [
  {
    id: 2,
    name: "name",
    header: "نام",
  },
  {
    id: 3,
    name: "lastName",
    header: "نام خانوادگی",
  },
  {
    id: 4,
    name: "mobile",
    header: "شماره همراه",
  },
  {
    id: 5,
    name: "email",
    header: "پست الکترونیکی",
  },
];
