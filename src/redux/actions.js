import { createAction } from "@reduxjs/toolkit";
import { OPEN_CLOSE, GET_INFO } from "./actionTypes";

export const OpenClose = createAction(OPEN_CLOSE);
export const GetInfo = (payload) => ({
  type: GET_INFO,
  payload
});
