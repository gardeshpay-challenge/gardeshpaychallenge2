import { createStore } from "redux";
import reducerFunc from './reducers/reducerFuncs';

export default createStore(reducerFunc);