import { OPEN_CLOSE, GET_INFO } from '../actionTypes';

const initialState = { opened: false, data: {} };

export default function reducerFunc(state = initialState, actions) {
  switch(actions.type) {
    case OPEN_CLOSE:
      return {
        ...state,
        opened: !state.opened
      }
      case GET_INFO:
        return {
          ...state,
          data: actions.payload
        }
      default:
        return state;
  }
}